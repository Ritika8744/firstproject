package com.jenkins.jenkins.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("hello")
public class HelloWorld {
	
	@GetMapping
	public String getMsg(){
		return "Hello World..!";
	}

}
